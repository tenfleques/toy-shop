// JavaScript Document
function productCard(){
	//needed, name, description, path to picture,price
	var product = {};
	if(arguments[0])
		product = arguments[0];
	else
		return "";
	var buttonWidth = arguments[1]?"col-10":"col-5";	
	var sexstring = "";
	var agestring = "";
	
	for(var i = 0; i < product.sex.length; ++i)
		sexstring += " "+product.sex[i];
	for(var i = 0; i < product.age.length; ++i)
		agestring += " a-"+product.sex[i];
	
	var html = "";
	html += '<div class="mb-3 border-primary sex-filter card '+agestring+' '+sexstring+'">' //use the classes sexstring and agestring for filtering
	html += 	'<img class="card-img-top" src="'+product.path+'" alt="'+product.title+'">'
	html +=		'<div class="card-body">'
	html +=			'<h5 class="card-title">'+product.title+'</h5>'
	//html +=			'<p class="card-text">'+product.description+'</p>'
	html +=		'</div>'
	html +=		'<div class="card-footer">'
	html +=			'<h6 class="text-muted text-center price">'+product.price+' <i class="fa fa-ruble"></i></h6>'
	html +=			'<div class="row">'
	html +=			'<button class="btn btn-success m-2 '+buttonWidth+'">купить</button>'
	html +=			'<button class="btn btn-info m-2 '+buttonWidth+'">подробнее</button></div>'
	html +=		'</div>'
	html +=	'</div>';
	return html;
}
function loadProducts(){
	var products = arguments[0];
	var html = "";
	for (var i =0; i< products.length; ++i){
		html += productCard(products[i],arguments[1]);
	}
	return html;
}
function carouselItem(){
	if(!arguments[0])
		return "";
	var active = arguments[1];
	var carItem = arguments[0];
	html = '<div class="carousel-item '+active+'">'
	html += ' <img src="'+carItem.img+'" class="d-block w-100" alt="'+carItem.title+'">'
	html += '	  <div class="carousel-caption d-none d-md-block">'
	html += '		<h5>'+carItem.title+'</h5>'
	html += '		<p>'+carItem.details+'</p>'
	html += '	  </div>'
	html += '	</div>';
	return html;
}
function carousel(){
	var carItems = [
		{img: "img/carousel/mini-park.jpg", title: "главо", details: "описание о парке"},
		{img: "img/carousel/red-car.jpg", title: "красная машина", details: "описание о машине"},
		{img: "img/carousel/scooter.jpg", title: "скутер", details: "описание о скутере"}
	];
	var html = "";
	var active = "active"
	for(var i = 0;  i < carItems.length; ++i){
		html += carouselItem(carItems[i], active);
		active = "";
	}
	return html;
		
}
$(function(){
	//load the toys to the main page
	var products = [
		{title: "red car", description: "вот это парк", path: "/img/products/all/mini-park.jpg", sex: ["all"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "mini-park", description: "вот это машина ", path: "/img/products/boys/red-car.jpg", sex: ["boys"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "scooter", description: "вот это скутер", path: "/img/products/all/scooter.jpg", sex: ["all"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "woody", description: "вот это woody", path: "/img/products/boys/woody.jpg", sex: ["boys"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "барбые", description: "вот это барбые", path: "/img/products/girls/barbie-stuff.jpg", sex: ["girls"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "телетубые", description: "вот это сумка телетубые", path: "/img/products/girls/teletubbies-lunch-bag.jpg", sex: ["girls"], age: [3,4,5,6,7,8,9,10], price:1000}
	];
	
	var popularProducts = [
		{title: "red car", description: "вот это парк", path: "/img/products/all/mini-park.jpg", sex: ["all"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "mini-park", description: "вот это машина ", path: "/img/products/boys/red-car.jpg", sex: ["boys"], age: [3,4,5,6,7,8,9,10], price:1000},
		{title: "scooter", description: "вот это скутер", path: "/img/products/girls/scooter.jpg", sex: ["girls"], age: [3,4,5,6,7,8,9,10], price:1000}
	];
	
	
	var title = "детскые игрушки";
	$("title, .title").html(title);
	$(".copyrights-date").html(new Date().getFullYear())
	$(".carousel-inner").html(carousel());
	$(".main-view-toys").html(loadProducts(products));
	$(".popular-toys").html(loadProducts(popularProducts,true))
})